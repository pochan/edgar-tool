﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;


namespace SentenmentAnalyzer
{
	public class Analyzer
	{
		static List<string> _pluseOneWords;
		static List<string> _minueOneWords;

		static void LoadDictionary()
		{
			var currentDir = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location );

			var positiveWordsPath = Path.Combine( currentDir, "PositiveWords.txt" );
			_pluseOneWords = new List<string>();

			foreach ( var line in File.ReadLines( positiveWordsPath ) )
			{
				_pluseOneWords.Add( line );
			}

			var negativeWordsPath = Path.Combine( currentDir, "NegativeWords.txt" );
			_minueOneWords = new List<string>();
			foreach ( var line in File.ReadLines( negativeWordsPath ) )
			{
				_minueOneWords.Add( line );
			}
		}

		public static double AnalyzeWith_RScript( string articalToAnalyze, string rScriptExe, string rScriptPath )
		{
			rScriptExe = @"C:\Dev\8kAnalyzer\8kAnalyzer\bin\Debug\R-3.4.2\bin\Rscript.exe";
			var stringResult = RScriptRunner.RunFromCmd( rScriptPath, rScriptExe, articalToAnalyze ).Replace("[1] ", string.Empty);

			double.TryParse( stringResult, out double result );
			return result;
		}

		public static double Analyze( string articalToAnalyze )
		{
			if ( _pluseOneWords == null || _minueOneWords == null )
				LoadDictionary();

			var words = articalToAnalyze.Split( ' ' )
				.Where(s=>!string.IsNullOrWhiteSpace(s) )
				.Where(s=>s.Length>=3)
				.ToList();

			var positiveWords = words
				.Where( w => _pluseOneWords.Exists( pw => string.Compare( w, pw, StringComparison.OrdinalIgnoreCase ) == 0 ) ).ToList();
			var negativeWords = words
				.Where( w => _minueOneWords.Exists( nw => string.Compare( w, nw, StringComparison.OrdinalIgnoreCase ) == 0 ) ).ToList();

			return positiveWords.Count / (double) words.Count - negativeWords.Count / (double) words.Count;
		}

		public static string GetCleanedEdgarText( string input )
		{
			var firstDocument = GetElementByTag(input, "DOCUMENT");
			var text = GetElementByTag(firstDocument, "TEXT");

			var cleanedText = Regex.Replace( text, "<.*?>", String.Empty ).Trim().Replace("&nbsp;", " ");
			cleanedText = Regex.Replace( cleanedText, @"[^0-9a-zA-Z]+", " " );
			return Regex.Replace( cleanedText, @"[0-9\-]", string.Empty );
		}

		static string GetElementByTag( string input, string tag )
		{
			var result = string.Empty;
			var indicies = GetIndiciesByName(input, tag, 0);

			if ( indicies.IsValid )
			{
				result = input.Substring( indicies.StartIdx, indicies.Length ).Trim();
			}

			return result;
		}

		static TagIndicies GetIndiciesByName( string input, string tag, int offset )
		{
			int startIdx = input.IndexOf($"<{tag}>", offset);
			int endIdx =  input.IndexOf($"</{tag}>", offset);
			return new TagIndicies( startIdx, endIdx, tag );
		}

		class TagIndicies
		{
			string Tag;
			public TagIndicies( int startIdx, int endIdx, string tag )
			{
				StartIdx = startIdx;
				EndIdx = endIdx;
				Tag = tag;
			}

			public int StartIdx, EndIdx;
			public bool IsValid
			{
				get { return StartIdx >= 0 && EndIdx >= 0 && EndIdx > StartIdx; }
			}

			public int Length { get { return EndIdx + ( Tag.Length + 3 ) - StartIdx; } }
		}
	}

	internal class RScriptRunner
	{
		public static string RunFromCmd( string rCodeFilePath, string rScriptExecutablePath, string args )
		{
			string file = rCodeFilePath;
			string result = string.Empty;

			try
			{

				var info = new ProcessStartInfo();
				info.FileName = rScriptExecutablePath;
				info.WorkingDirectory = Path.GetDirectoryName( rScriptExecutablePath );
				info.Arguments = rCodeFilePath + " " + args;
				info.RedirectStandardInput = false;
				info.RedirectStandardOutput = true;
				info.UseShellExecute = false;
				info.CreateNoWindow = true;

				using ( var proc = new Process() )
				{
					proc.StartInfo = info;
					proc.Start();
					result = proc.StandardOutput.ReadToEnd();
				}

				return result;
			}
			catch ( Exception ex )
			{
				throw new Exception( "R Script failed: " + result, ex );
			}
		}
	}
}