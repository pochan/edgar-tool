if (!require("pacman")) install.packages("pacman")
pacman::p_load("syuzhet")

library(syuzhet)
library(stringr)

args <- commandArgs(trailingOnly = TRUE)

inputString <- paste(args, collapse = ' ')

sentiment = get_sentiment(inputString)

print(sentiment)