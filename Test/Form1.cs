﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SentenmentAnalyzer;

namespace Test
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			test();
		}

		void test()
		{
			string readText = File.ReadAllText(@"C:\tmp\test.nc");
			var cleanedTxt = Analyzer.GetCleanedEdgarText( readText );
			var sentenment = Analyzer.AnalyzeWith_RScript(cleanedTxt, @"C:\Dev\8kAnalyzer\8kAnalyzer\bin\Debug\Sentenment.R", @"C:\Dev\8kAnalyzer\8kAnalyzer\bin\Debug\Sentenment.R");
		}
	}
}
