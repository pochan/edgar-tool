﻿using System.Data.SQLite;

namespace EdgarAnalyzer {
	public class Constants { 
		public const string SQL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
		public const string TEST_SUMMARY_DB_PATH = @"C:\tmp\summary.db";
		public const string SUMMARY_DB_CONNECTION_STRING = "Data Source = " + TEST_SUMMARY_DB_PATH + "; Version=3; Connection Timeout=30";

		public const string STENMENT_SUMMARY_DB_PATH = @"C:\tmp\SentenmentSummary.db";
		public const string STENMENT_DB_CONNECTION_STRING = "Data Source = " + STENMENT_SUMMARY_DB_PATH + "; Version=3; Connection Timeout=30";




	}
}
