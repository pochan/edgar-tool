﻿namespace EdgarAnalyzer
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
         System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
         void InitializeComponent()
        {
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.downloadDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.processFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.verifyDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.verifyAllDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.processFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sentenmentAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.kToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.dataSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.createSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.clearSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toCsvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.downloadStatusTextBox = new System.Windows.Forms.TextBox();
			this.downloadStatusPanel = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.menuStrip1.SuspendLayout();
			this.downloadStatusPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataToolStripMenuItem,
            this.settingsToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(284, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// dataToolStripMenuItem
			// 
			this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadDataToolStripMenuItem,
            this.processFilesToolStripMenuItem,
            this.dataSummaryToolStripMenuItem,
            this.toCsvToolStripMenuItem});
			this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
			this.dataToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
			this.dataToolStripMenuItem.Text = "Data";
			// 
			// downloadDataToolStripMenuItem
			// 
			this.downloadDataToolStripMenuItem.Name = "downloadDataToolStripMenuItem";
			this.downloadDataToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.downloadDataToolStripMenuItem.Text = "Download Data";
			this.downloadDataToolStripMenuItem.ToolTipText = "Download daily filings from Edgar(Use only 1 thread for this task)";
			this.downloadDataToolStripMenuItem.Click += new System.EventHandler(this.downloadDataToolStripMenuItem_Click);
			// 
			// processFilesToolStripMenuItem
			// 
			this.processFilesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verifyDataToolStripMenuItem,
            this.verifyAllDataToolStripMenuItem,
            this.processFileToolStripMenuItem,
            this.sentenmentAnalysisToolStripMenuItem});
			this.processFilesToolStripMenuItem.Name = "processFilesToolStripMenuItem";
			this.processFilesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.processFilesToolStripMenuItem.Text = "Process Files";
			// 
			// verifyDataToolStripMenuItem
			// 
			this.verifyDataToolStripMenuItem.Name = "verifyDataToolStripMenuItem";
			this.verifyDataToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.verifyDataToolStripMenuItem.Text = "Verify Downloaded Data";
			this.verifyDataToolStripMenuItem.Click += new System.EventHandler(this.VerifyDataToolStripMenuItem_Click);
			// 
			// verifyAllDataToolStripMenuItem
			// 
			this.verifyAllDataToolStripMenuItem.Name = "verifyAllDataToolStripMenuItem";
			this.verifyAllDataToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.verifyAllDataToolStripMenuItem.Text = "Verify All Data";
			this.verifyAllDataToolStripMenuItem.ToolTipText = "Verify all file starting from 1995 to current date.  Will attemp to redownload th" +
    "e file if it is missing or incomplete";
			this.verifyAllDataToolStripMenuItem.Click += new System.EventHandler(this.VerifyAllDataToolStripMenuItem_Click);
			// 
			// processFileToolStripMenuItem
			// 
			this.processFileToolStripMenuItem.Name = "processFileToolStripMenuItem";
			this.processFileToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.processFileToolStripMenuItem.Text = "Process filings";
			this.processFileToolStripMenuItem.ToolTipText = "Calculates sentenment score on 8k filings and and append \'10k\' or \'10Q\'  to 10K o" +
    "r 10Q filings";
			this.processFileToolStripMenuItem.Click += new System.EventHandler(this.processFilesToolStripMenuItem_Click);
			// 
			// sentenmentAnalysisToolStripMenuItem
			// 
			this.sentenmentAnalysisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kToolStripMenuItem});
			this.sentenmentAnalysisToolStripMenuItem.Name = "sentenmentAnalysisToolStripMenuItem";
			this.sentenmentAnalysisToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.sentenmentAnalysisToolStripMenuItem.Text = "Sentenment Analysis";
			// 
			// kToolStripMenuItem
			// 
			this.kToolStripMenuItem.Name = "kToolStripMenuItem";
			this.kToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.kToolStripMenuItem.Text = "10K, 10Q, 8K";
			this.kToolStripMenuItem.ToolTipText = "Calculates sentenment scores starts with the date specified.  Result is stored in" +
    " a sql database";
			this.kToolStripMenuItem.Click += new System.EventHandler(this.Perform10KAnalysis_Click);
			// 
			// dataSummaryToolStripMenuItem
			// 
			this.dataSummaryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createSummaryToolStripMenuItem,
            this.clearSummaryToolStripMenuItem});
			this.dataSummaryToolStripMenuItem.Name = "dataSummaryToolStripMenuItem";
			this.dataSummaryToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.dataSummaryToolStripMenuItem.Text = "Data Summary";
			// 
			// createSummaryToolStripMenuItem
			// 
			this.createSummaryToolStripMenuItem.Name = "createSummaryToolStripMenuItem";
			this.createSummaryToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.createSummaryToolStripMenuItem.Text = "New Summary";
			this.createSummaryToolStripMenuItem.ToolTipText = "Create a new summary database based on file downloaded";
			this.createSummaryToolStripMenuItem.Click += new System.EventHandler(this.createSummaryToolStripMenuItem_Click);
			// 
			// clearSummaryToolStripMenuItem
			// 
			this.clearSummaryToolStripMenuItem.Name = "clearSummaryToolStripMenuItem";
			this.clearSummaryToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.clearSummaryToolStripMenuItem.Text = "Clear Summary";
			this.clearSummaryToolStripMenuItem.ToolTipText = "Delet summary database";
			// 
			// toCsvToolStripMenuItem
			// 
			this.toCsvToolStripMenuItem.Name = "toCsvToolStripMenuItem";
			this.toCsvToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.toCsvToolStripMenuItem.Text = "ToCsv";
			this.toCsvToolStripMenuItem.ToolTipText = "Converts sentenment database to csv format";
			this.toCsvToolStripMenuItem.Click += new System.EventHandler(this.toCsvToolStripMenuItem_Click);
			// 
			// settingsToolStripMenuItem
			// 
			this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
			this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
			this.settingsToolStripMenuItem.Text = "Settings";
			this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// downloadStatusTextBox
			// 
			this.downloadStatusTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadStatusTextBox.Enabled = false;
			this.downloadStatusTextBox.Location = new System.Drawing.Point(3, 20);
			this.downloadStatusTextBox.Multiline = true;
			this.downloadStatusTextBox.Name = "downloadStatusTextBox";
			this.downloadStatusTextBox.Size = new System.Drawing.Size(253, 163);
			this.downloadStatusTextBox.TabIndex = 1;
			// 
			// downloadStatusPanel
			// 
			this.downloadStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadStatusPanel.Controls.Add(this.label1);
			this.downloadStatusPanel.Controls.Add(this.downloadStatusTextBox);
			this.downloadStatusPanel.Location = new System.Drawing.Point(13, 64);
			this.downloadStatusPanel.Name = "downloadStatusPanel";
			this.downloadStatusPanel.Size = new System.Drawing.Size(259, 186);
			this.downloadStatusPanel.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(4, 4);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(103, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Download Status";
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Location = new System.Drawing.Point(12, 38);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
			this.dateTimePicker1.TabIndex = 3;
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.Controls.Add(this.dateTimePicker1);
			this.Controls.Add(this.downloadStatusPanel);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Main";
			this.Text = "Edger Tool";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.downloadStatusPanel.ResumeLayout(false);
			this.downloadStatusPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

         System.Windows.Forms.MenuStrip menuStrip1;
         System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
         System.Windows.Forms.ToolStripMenuItem downloadDataToolStripMenuItem;
         System.Windows.Forms.OpenFileDialog openFileDialog1;
         System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
         System.Windows.Forms.ToolStripMenuItem processFilesToolStripMenuItem;
         System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
         System.Windows.Forms.TextBox downloadStatusTextBox;
         System.Windows.Forms.Panel downloadStatusPanel;
         System.Windows.Forms.Label label1;
         System.Windows.Forms.ToolStripMenuItem dataSummaryToolStripMenuItem;
         System.Windows.Forms.ToolStripMenuItem createSummaryToolStripMenuItem;
         System.Windows.Forms.ToolStripMenuItem clearSummaryToolStripMenuItem;
         System.Windows.Forms.ToolStripMenuItem verifyDataToolStripMenuItem;
         System.Windows.Forms.ToolStripMenuItem processFileToolStripMenuItem;
     System.Windows.Forms.ToolStripMenuItem verifyAllDataToolStripMenuItem;
		 System.Windows.Forms.ToolStripMenuItem sentenmentAnalysisToolStripMenuItem;
		 System.Windows.Forms.ToolStripMenuItem kToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toCsvToolStripMenuItem;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
	}
}

