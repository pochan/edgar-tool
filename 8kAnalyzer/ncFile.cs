﻿using SentenmentAnalyzer;
using System;
using System.IO;
using EdgarAnalyzer.Properties;

namespace EdgarAnalyzer
{
	public class FilingData
	{
		public string FilingType;
		string _text;
		public string Text
		{
			get { return _text; }
			set
			{
				_text = value;

				if ( Settings.Default.UseRSentenmentAnalyzer )
				{
					_sentenment = Analyzer.AnalyzeWith_RScript( Text, Settings.Default.RScriptExe, Settings.Default.RScriptPath );
				}

				else
					_sentenment = Analyzer.Analyze( Text );
			}
		}

		public string CompanyName;
		public DateTime FilingDate;
		public double Sentenment
		{
			get
			{
				if ( double.IsNaN( _sentenment ) )
				{
					if ( Settings.Default.UseRSentenmentAnalyzer )
					{
						_sentenment = Analyzer.AnalyzeWith_RScript( Text, Settings.Default.RScriptExe, Settings.Default.RScriptPath );
					}

					else
					_sentenment = Analyzer.Analyze( Text );
				}
				return _sentenment;
			}
		}

		public long CIK;
		public long irsNumber;

		double _sentenment = double.NaN;

		public static string GetFilingType( string filePath )
		{
			using ( StreamReader sr = File.OpenText( filePath ) )
			{
				var line = string.Empty;
				while ( ( line = sr.ReadLine() ) != null )
				{
					if ( line.Substring( 0, 6 ) == "<TYPE>" )
					{
						return line.Substring( 6 ).Trim().ToUpper();
					}
				}
			}
			return "Unknown";
		}

		public static FilingSummary GetFilingSummary( string filePath )
		{
			var fs = new FilingSummary();

			using ( StreamReader sr = File.OpenText( filePath ) )
			{
				fs.FileName = filePath;
				string line = string.Empty;
				while ( ( line = sr.ReadLine() ) != null )
				{
					if ( line.Length > 6 && line.Substring( 0, 6 ) == "<TYPE>" )
					{
						fs.FilingType = line.Substring( 6 ).Trim();
					}

					else if ( line.Length > 14 && line.Substring( 0, 13 ) == "<FILING-DATE>" )
					{
						try
						{
							var dateStr = line.Substring( 13 ).Trim();
							var dateYear = dateStr.Substring( 0, 4 );
							var dateMonth = dateStr.Substring( 4, 2 );
							var dateDate = dateStr.Substring( 6, 2 );
							var year = int.Parse( dateYear );
							var month = int.Parse( dateMonth );
							var date = int.Parse( dateDate );
							fs.FilingDate = new DateTime( year, month, date );
						}
						catch ( Exception ex )
						{ }
					}

					else if ( line.Length > 17 && line.Substring( 0, 16 ) == "<CONFORMED-NAME>" )
					{
						fs.CompanyName = line.Substring( 16 ).Trim();
					}
				}
			}
			return fs;
		}

		public static FilingData GetFilingData( string filePath )
		{
			var confirmNameLength = "<CONFORMED-NAME>".Length;
			var companyName = string.Empty;
			var date = DateTime.MinValue;
			var filingType = "Unknown";
			long cik = 0;
			long irsNumber = 0;
			bool typeAlreadyRead = false;
			bool cikAlreadyRead = false;

			using ( var sr = File.OpenText( filePath ) )
			{
				var line = string.Empty;
				while ( ( line = sr.ReadLine() ) != null )
				{
					if ( line.Length >= confirmNameLength && line.Substring( 0, confirmNameLength ) == "<CONFORMED-NAME>" )
					{
						companyName = line.Substring( confirmNameLength ).Trim();
					}

					else if ( line.Length > 6 && line.Substring( 0, 6 ) == "<TYPE>" && !typeAlreadyRead )
					{
						typeAlreadyRead = true;
						filingType = line.Substring( 6 ).Trim().ToUpper();
					}


					else if ( line.Length >= 13 && line.Substring( 0, 13 ) == "<FILING-DATE>" )
					{
						date = ParseDate( line.Substring( 13 ).Trim() );
					}


					else if ( line.Length >= 5 && line.Substring( 0, 5 ) == "<CIK>" && !cikAlreadyRead )
					{
						cikAlreadyRead = true;
						long.TryParse( line.Substring( 5 ).Trim(), out cik );
					}

					else if ( line.Length >= 12 && line.Substring( 0, 12 ) == "<IRS-NUMBER>" )
					{
						long.TryParse( line.Substring( 12 ).Trim(), out irsNumber );
					}				
				}			
			}

			var completeDoc = File.ReadAllText(filePath);
			var texts = Analyzer.GetCleanedEdgarText( completeDoc );

			var data = new FilingData
			{
				Text = texts,
				FilingType = filingType,
				CompanyName = companyName,
				FilingDate = date,
				CIK = cik,
				irsNumber = irsNumber
			};

			return data;
		}

		public static DateTime ParseDate( string dateStr )
		{
			int year = int.Parse( dateStr.Substring( 0, 4 ) ),
				month = int.Parse( dateStr.Substring( 4, 2 ) ),
				day = int.Parse( dateStr.Substring( 6, 2 ) );

			return new DateTime( year, month, day );
		}

		public override string ToString()
		{
			return FilingDate.ToShortDateString() + ", " + CompanyName + ", " + Sentenment.ToString();
		}
	}

	public class FilingSummary
	{
		public DateTime FilingDate;
		public string FilingType;
		public string CompanyName;
		public string FileName;

		public string ToCsv()
		{
			return $"{CompanyName},{FilingType},{FilingDate.ToShortDateString()}";
		}

		public const string CsvHeader = "Company Name, Filing Type, Date";
	}

	public enum FilingType
	{
		_10_K = 0,
		_10_Q = 1,
		_8K = 2,
	}
}
