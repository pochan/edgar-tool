﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Windows.Forms;
using EdgarAnalyzer.Properties;
using EdgarAnalyzer.Tasks;

namespace EdgarAnalyzer
{
	public partial class Main : Form
	{
		const string TEST_SUMMARY_DB = @"C:\tmp\EdgarSummary";
		const string TEST_SUMMARY_CSV = @"C:\tmp\EdgarSummary.csv";
		const string TEST_EDGAR_DIR = @"C:\Edgar Filings";
		const string TEST_EDGAR_FILE = @"C:\Edgar Filings\1995_9_15.nc.tar.gz";

		#region Properties and fields


		List<FilingData> filingSentenments;
		readonly SettingForm settingsForm;
		TaskProcessor dlProcessor;
		#endregion

		public Main()
		{
			InitializeComponent();
			settingsForm = new SettingForm();
			downloadStatusPanel.Visible = false;
		}


		public void UpdateDownLoadStatus( DownLoadStatus dlStatus )
		{
			downloadStatusTextBox.Invoke( (MethodInvoker) delegate
			{
				// Running on the UI thread
				downloadStatusTextBox.Text = dlStatus.ToString();
			} );
		}

		void downloadDataToolStripMenuItem_Click( object sender, EventArgs e )
		{
			folderBrowserDialog1.Description = "Select output directory";
			if ( folderBrowserDialog1.ShowDialog() != DialogResult.OK )
				return;

			if ( dlProcessor != null && dlProcessor.InProgress )
			{
				MessageBox.Show( "Download already started." );
				return;
			}

			downloadStatusPanel.Visible = true;

			dlProcessor = new TaskProcessor( Settings.Default.ThreadCount, this );
			dlProcessor.StartTasks( dateTimePicker1.Value, folderBrowserDialog1.SelectedPath, TaskType.Download );
		}

		void processFilesToolStripMenuItem_Click( object sender, EventArgs e )
		{
			folderBrowserDialog1.Description = "Select directory containing zip files";
			if ( folderBrowserDialog1.ShowDialog() != DialogResult.OK )
				return;

			filingSentenments = new List<FilingData>();
			ProcessAllData( folderBrowserDialog1.SelectedPath );
		}

		void ProcessAllData( string ZipFileDirectory )
		{
			var directory = new DirectoryInfo( ZipFileDirectory );
			var files = directory.GetFiles( "*.gz" );
			foreach ( var zipFile in files )
				ProcessZipFile( zipFile.FullName, directory.ToString() );

			saveOutput( directory.Parent.FullName );
		}

		void ProcessZipFile( string gzipFileName, string dir )
		{
			var destination = Util.UnzipEdgarFile( gzipFileName, dir );

			ProcessExtractedFiles( destination );
		}

		void ProcessExtractedFiles( string fileDirectory )
		{
			var directory = new DirectoryInfo( fileDirectory );
			var files = directory.GetFiles( "*.nc" ); // TODO need to process .corr files too??

			foreach ( var file in files )
				ProcessNcFile( file.FullName );
		}

		void saveOutput( string fileDirectory )
		{
			var outputPath = Path.Combine( fileDirectory, "8k Sentenments.txt" );

			using ( var sw = new StreamWriter( outputPath ) )
			{
				foreach ( var filingSentenment in filingSentenments )
					sw.WriteLine( filingSentenment.ToString() );
			}
		}

		void ProcessNcFile( string ncFilePath )
		{
			var filingType = FilingData.GetFilingType( ncFilePath );
			switch ( filingType )
			{
				case "8-K":
					var _8_K_File = RenameFileName( ncFilePath, ".8K" );
					Process8KData( _8_K_File );
					break;

				case "10-K":
					RenameFileName( ncFilePath, ".10K" );
					break;

				case "10-Q":
					RenameFileName( ncFilePath, ".10Q" );
					break;

				default:
					File.Delete( ncFilePath );
					break;
			}
		}

		void Process8KData( string filePath )
		{
			var filing = FilingData.GetFilingData( filePath );
			filingSentenments.Add( filing );
		}

		string RenameFileName( string oldfilename, string extension )
		{
			var fi = new FileInfo( oldfilename );
			var newName = fi.Name + extension;
			var newFileName = Path.Combine( fi.DirectoryName, newName );

			Util.MoveFile( newFileName, oldfilename );

			return newFileName;
		}


		void settingsToolStripMenuItem_Click( object sender, EventArgs e )
		{
			settingsForm.ShowDialog();
		}

		void CreateNewSummaryCSV( string edgarFile )
		{
			var outputDirectory = Util.UnzipEdgarFile( TEST_EDGAR_FILE, "C:\\tmp" );

			var directory = new DirectoryInfo( outputDirectory );
			var files = directory.GetFiles( "*.nc" ); // TODO need to process .corr files too

			var summaryList = new List<FilingSummary>();

			if ( File.Exists( TEST_SUMMARY_CSV ) )
				File.Delete( TEST_SUMMARY_CSV );

			using ( var writetext = new StreamWriter( TEST_SUMMARY_CSV ) )
			{
				writetext.WriteLine( FilingSummary.CsvHeader );
				foreach ( var file in files )
				{
					var summary = FilingData.GetFilingSummary( file.FullName );
					writetext.WriteLine( summary.ToCsv() );
				}
			}
		}

		void UpdateSummaryDb( string edgarFile )
		{
			var fi = new FileInfo( edgarFile );
			var outputDirectory = Util.UnzipEdgarFile( edgarFile, "C:\\tmp\\" + fi.Name );
			var directory = new DirectoryInfo( outputDirectory );

			var files = directory.GetFiles( "*.nc" ); // TODO need to process .corr files too

			var summaryList = new List<FilingSummary>();

			if ( File.Exists( TEST_SUMMARY_CSV ) )
				File.Delete( TEST_SUMMARY_CSV );

			using ( var connection = new SQLiteConnection( Constants.SUMMARY_DB_CONNECTION_STRING ) )
			{
				connection.Open();
				foreach ( var file in files )
				{
					var summary = FilingData.GetFilingSummary( file.FullName );
					WriteToSummaryDb( connection, summary );
				}
			}

			try
			{
				var fileToCleanUp = directory.Parent.FullName;
				Directory.Delete( fileToCleanUp, true );
			}
			catch ( Exception ex ) { }
		}

		void WriteToSummaryDb( SQLiteConnection conn, FilingSummary summary )
		{
			using ( var cmd = new SQLiteCommand( conn ) )
			{
				cmd.CommandText =
					$@"INSERT INTO FilingSummary (company, fileType, date) VALUES ('{summary.CompanyName}', '{summary.FilingType}', '{
							summary.FilingDate.ToString( Constants.SQL_DATE_FORMAT )
						}')";
				cmd.Prepare();
				try
				{
					cmd.ExecuteNonQuery();
				}
				catch ( Exception ex ) { }
			}
		}


		static void CreateNewDb()
		{
			SQLiteConnection.CreateFile( Constants.TEST_SUMMARY_DB_PATH );

			var result = -1;
			using ( var conn = new SQLiteConnection( Constants.SUMMARY_DB_CONNECTION_STRING ) )
			{
				conn.Open();
				using ( var cmd = new SQLiteCommand( conn ) )
				{
					cmd.CommandText = "CREATE table FilingSummary (company varchar(50), fileType varchar(50), date datetime )";
					cmd.Prepare();
					try
					{
						result = cmd.ExecuteNonQuery();
					}
					catch ( SQLiteException ) { }
				}

				conn.Close();
			}
		}

		void createSummaryToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if ( folderBrowserDialog1.ShowDialog() != DialogResult.OK )
				return;

			var directory = new DirectoryInfo( folderBrowserDialog1.SelectedPath );
			var files = directory.GetFiles( "*.tar.gz" );


			if ( File.Exists( Constants.TEST_SUMMARY_DB_PATH ) )
				File.Delete( Constants.TEST_SUMMARY_DB_PATH );

			CreateNewDb();

			foreach ( var file in files )
				UpdateSummaryDb( file.FullName );
		}

		bool GetWorkingDirectory()
		{
			if ( !string.IsNullOrEmpty( Settings.Default.DefaultEdgarDirectory ) )
			{
				folderBrowserDialog1.SelectedPath = Settings.Default.DefaultEdgarDirectory;
			}
			else if ( folderBrowserDialog1.ShowDialog() != DialogResult.OK )
			{
				return false;
			}
			return true;

		}

		void VerifyDataToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if ( !GetWorkingDirectory() )
			{
				return;
			}

			var directory = new DirectoryInfo( folderBrowserDialog1.SelectedPath );
			var files = directory.GetFiles( "*nc.tar.gz" );

			for ( var i = 0; i < files.Length; i++ )
				Util.VerifyDownloadedFile( files[i] );
		}

		void VerifyAllDataToolStripMenuItem_Click( object sender, EventArgs e )
		{
			folderBrowserDialog1.Description = "Select directory to verify";
			if ( folderBrowserDialog1.ShowDialog() != DialogResult.OK )
				return;

			if ( dlProcessor != null && dlProcessor.InProgress )
			{
				MessageBox.Show( "Download already started." );
				return;
			}

			downloadStatusPanel.Visible = true;

			dlProcessor = new TaskProcessor( Settings.Default.ThreadCount, this );
			dlProcessor.StartTasks( dateTimePicker1.Value, folderBrowserDialog1.SelectedPath, TaskType.Verify );
		}


		void Perform10KAnalysis_Click( object sender, EventArgs e )
		{
			if ( !GetWorkingDirectory() )
			{
				return;
			}

			if ( MessageBox.Show( "Delete existing data?", "Please Confirm", MessageBoxButtons.OKCancel ) == DialogResult.OK )
			{
				ConnectionHelper.CreateNewSentenmentSummaryDb();
			}

			dlProcessor = new TaskProcessor( Settings.Default.ThreadCount, this );
			dlProcessor.StartTasks( dateTimePicker1.Value, folderBrowserDialog1.SelectedPath, TaskType.Create10kSentenments );

		}

		void ConvertToCsv( string outFile )
		{
			var conn = ConnectionHelper.CommonSqlConnection( Constants.STENMENT_DB_CONNECTION_STRING );

			StringBuilder str = new StringBuilder( "company;fileType;date;sentenment;CIK;IRS_Numb");

			using ( var cmd = new SQLiteCommand( conn ) )
			{
				cmd.CommandText = @"SELECT * FROM Sentenments order by date desc";
				//cmd.CommandText = @"SELECT * FROM Sentenments";
				cmd.Prepare();
				try
				{
					SQLiteDataReader r = cmd.ExecuteReader();
					while ( r.Read() )
					{
						var company = Convert.ToString( r["company"] ) ;
						var fileType = Convert.ToString( r["fileType"] ) ;
						var date = Convert.ToString( r["date"] ) ;
						var sentenment = Convert.ToString( r["sentenment"] ) ;
						var CIK = Convert.ToString( r["CIK"] ) ;
						var IRS_Numb = Convert.ToString( r["IRS_Numb"] ) ;

						str.Append( $"{Environment.NewLine}{company};{fileType};{date};{sentenment};{CIK};{IRS_Numb}" );
					}
				}
				catch ( Exception ex ) { }
				File.WriteAllText( outFile, str.ToString() );
			}
		}

		private void toCsvToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if ( saveFileDialog1.ShowDialog() != DialogResult.OK )
				return;

			ConvertToCsv( saveFileDialog1.FileName );

		}
	}
}