﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdgarAnalyzer
{
	public class ConnectionHelper
	{

		static ConcurrentDictionary<string, SQLiteConnection> connections = new ConcurrentDictionary<string, SQLiteConnection>();

		public static SQLiteConnection CommonSqlConnection( string dbConnectionString )
		{
			if ( !connections.ContainsKey( dbConnectionString ) )
			{
				var connection = new SQLiteConnection(dbConnectionString, true);
				connection.Open();
				connections.TryAdd( dbConnectionString, connection );
			}
			return connections[dbConnectionString];
		}

		public static void CreateNewSentenmentSummaryDb()
		{
			if ( File.Exists( Constants.STENMENT_SUMMARY_DB_PATH ) )
				File.Delete( Constants.STENMENT_SUMMARY_DB_PATH );

			SQLiteConnection.CreateFile( Constants.STENMENT_SUMMARY_DB_PATH );

			var result = -1;

			var conn = CommonSqlConnection(Constants.STENMENT_DB_CONNECTION_STRING);

			using ( var cmd = new SQLiteCommand( conn ) )
			{
				cmd.CommandText = "CREATE table Sentenments (company varchar(50), fileType varchar(50), date datetime, sentenment REAL, CIK BIGINT, IRS_Numb BIGINT)";
				cmd.Prepare();
				try
				{
					result = cmd.ExecuteNonQuery();
				}
				catch ( SQLiteException ) { }
			}
		}
	}
}
