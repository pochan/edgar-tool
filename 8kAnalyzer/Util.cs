﻿using System;
using System.IO;
using System.Net;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
#pragma warning disable 168

namespace EdgarAnalyzer
{
	public static class Util
	{
		public static bool MoveFile( string oldfilename, string newFileName )
		{
			try
			{
				if ( File.Exists( newFileName ) )
				{
					File.Delete( newFileName );
				}

				File.Move( oldfilename, newFileName );
				return true;
			}
			catch
			{ return false; }
		}

		public static void VerifyDownloadedFile( FileInfo fileInfo )
		{
			var tmpFolder =  Path.Combine( Directory.GetCurrentDirectory(), $"tmp_{fileInfo.Name}" );
			Directory.CreateDirectory( tmpFolder );
			var unzipResult = UnzipEdgarFile( fileInfo.FullName, Directory.GetCurrentDirectory() );

			if ( string.IsNullOrEmpty( unzipResult ) )
			{
				var url = GetUrlFromFile( fileInfo.Name );
				fileInfo.Delete();
				DownLoadFile( url, fileInfo.FullName );
			}
			Directory.Delete( unzipResult, true );
		}

		public static string UnzipEdgarFile( string gzipFileName, string dir )
		{
			var destination = Path.Combine( dir, "tmp" );

			var tarFileInfo = new FileInfo( gzipFileName );
			if ( !tarFileInfo.Exists )
			{
				return string.Empty;
			}

			var targetDirectory = new DirectoryInfo( destination );
			if ( !targetDirectory.Exists )
			{
				targetDirectory.Create();
			}
			try
			{
				using ( Stream sourceStream = new GZipInputStream( tarFileInfo.OpenRead() ) )
				{
					using ( TarArchive tarArchive = TarArchive.CreateInputTarArchive( sourceStream, TarBuffer.DefaultBlockFactor ) )
					{
						tarArchive.ExtractContents( targetDirectory.FullName );
					}
				}
			}
			catch ( Exception ex )
			{
				destination = string.Empty;
			}

			return destination;
		}

		public static bool DownLoadFile( string url, string outputPath )
		{
			using ( var client = new WebClient() )
			{
				try
				{
					var tmpFile = outputPath + ".tmp";
					client.DownloadFile( url, tmpFile );

					return MoveFile( tmpFile, outputPath );
				}
				catch ( Exception ex )
				{
					return false;
				}
			}
		}

		public static string GetUrlFromFile( string fileName )
		{
			int year, month, day, quarter;

			if ( !ExtractdateFromFileName( fileName, out year, out month, out day, out quarter ) )
				return string.Empty;

			var url = $"https://www.sec.gov/Archives/edgar/Feed/{year}/QTR{quarter}/{year}{ string.Format( "{0:00}", month )}{string.Format( "{0:00}", day )}.nc.tar.gz";
			return url;
		}

		static bool ExtractdateFromFileName( string fileName, out int year, out int month, out int day, out int quarter )
		{
			try
			{
				var datePartStr = fileName.Split( '.' )[0];
				var yearMonthDay = datePartStr.Split( '_' );
				year = int.Parse( yearMonthDay[0] );
				month = int.Parse( yearMonthDay[1] );
				day = int.Parse( yearMonthDay[2] );

				quarter = ( month + 2 ) / 3;
				return true;
			}
			catch
			{
				year = 0;
				month = 0;
				day = 0;
				quarter = 0;
				return false;
			}
		}
	}
}
