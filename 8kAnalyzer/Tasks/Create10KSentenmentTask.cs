﻿using System;
using System.Data.SQLite;
using System.IO;
using static EdgarAnalyzer.TaskProcessor;

namespace EdgarAnalyzer.Tasks
{
	public class Create10KSentenmentTask : DownloadTask
	{
		public override void StartTask()
		{
			Result = new TaskResult();

			try
			{


				var tmpFolder = Path.Combine( Directory.GetCurrentDirectory(), $"{Input.Date.Year}_{Input.Date.Month}_{Input.Date.Day}_Filings" );
				Directory.CreateDirectory( tmpFolder );
				var unzipResult = Util.UnzipEdgarFile( Input.OutputPath, tmpFolder );
				if ( string.IsNullOrEmpty( unzipResult ) )
				{
					Result.Status = TaskStatus.Failed;
				}
				else
				{
					Perform10KSentenmentAnalysis( unzipResult );
					Result.Status = TaskStatus.Completed;
				}

			}
			catch ( Exception ex )
			{
				Result.Status = TaskStatus.Failed;
			}

			RaiseTaskCompletedEvent();
		}

		void Perform10KSentenmentAnalysis( string unzippedEdgarFileDirectory )
		{
			var directory = new DirectoryInfo( unzippedEdgarFileDirectory );
			var files = directory.GetFiles( "*.nc" );

			foreach ( var file in files )
			{
				Perform10KSentenmentAnalysisOnFile( file.FullName );
			}
		}

		void Perform10KSentenmentAnalysisOnFile( string filePath )
		{
			var filingType = FilingData.GetFilingType( filePath );
			if ( filingType != "10-K" &&
				  filingType != "8-K" &&
				  filingType != "10-Q" )
			{
				return;
			}

			var filing = FilingData.GetFilingData( filePath );
			UpdateSummaryDataBase( filing );
		}

		void UpdateSummaryDataBase( FilingData filingData )
		{
			var conn = ConnectionHelper.CommonSqlConnection( Constants.STENMENT_DB_CONNECTION_STRING );

			using ( var cmd = new SQLiteCommand( conn ) )
			{
				cmd.CommandText =
					$@"INSERT INTO Sentenments (company, fileType, date, sentenment, CIK, IRS_Numb) VALUES (
						'{filingData.CompanyName}', 
						'{filingData.FilingType}', 
						'{ filingData.FilingDate.ToString( Constants.SQL_DATE_FORMAT ) }',
						'{filingData.Sentenment}',
						'{filingData.CIK}',
						'{filingData.irsNumber}'
					)";
				cmd.Prepare();
				try
				{
					cmd.ExecuteNonQuery();
				}
				catch ( Exception ex ) { }
			}
		}
	}

}
