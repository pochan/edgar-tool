﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using EdgarAnalyzer.Tasks;

#pragma warning disable 168

namespace EdgarAnalyzer
{
   public class TaskProcessor
   {
	  #region Properties and fields

	  readonly Main _mainForm;
	  ConcurrentQueue<IDownloadTask> _allDownloadTasks;
	  ConcurrentBag<TaskResult> _dlResults;

	  readonly int _maxThreadCount;
	  int _currentThreadCount;
	  DownLoadStatus _dlStatus;

	  #endregion

	  public delegate void DownloadCompletedEventHandler ( TaskResult downloadResult );

	  readonly object _downloadEventLock = new object();

	  readonly object _threadCountLock = new object();

	  public TaskProcessor ( int threadCount, Main caller )
	  {
		 _maxThreadCount = threadCount;
		 DownloadTask.TaskCompleted += HandleDownloadCompleteEvent;
		 _mainForm = caller;
	  }

	  public bool InProgress => _dlStatus.Progress >= 100;

	  public void StartTasks ( DateTime startDate, string outDirectory, TaskType taskType )
	  {

		 CreateTasks ( startDate, outDirectory, taskType );

		 _mainForm.UpdateDownLoadStatus ( _dlStatus );

		 for ( var i = 0; i < _maxThreadCount; i++ )
			TryStartOneTask ();
	  }

	  void TryStartOneTask ()
	  {
		 lock ( _threadCountLock )
		 {
			try
			{
			   if ( _currentThreadCount >= _maxThreadCount ||
					_allDownloadTasks.Count <= 0 )
				  return;

			   IDownloadTask downloadTask;
			   if ( _allDownloadTasks.TryDequeue ( out downloadTask ) )
			   {
				  _currentThreadCount++;
				  new Thread ( () =>
					{
					   Thread.CurrentThread.IsBackground = false;
					   Thread.CurrentThread.Priority = ThreadPriority.Highest;
					   downloadTask.StartTask ();
					} ).Start ();
			   }
			}
			catch ( Exception EX )
			{ }
		 }
	  }

	  void CreateTasks ( DateTime startDate, string outDirectory, TaskType taskType )
	  {
		 _allDownloadTasks = new ConcurrentQueue<IDownloadTask> ();
		 _dlResults = new ConcurrentBag<TaskResult> ();

		 var isFirstYear = true;

		 _dlStatus = new DownLoadStatus ();

		 for ( var year = startDate.Year; year <= DateTime.Now.Year; year++ )
		 {
			for ( var quarter = 1; quarter <= 4; quarter++ )
			{
			   var quartlyTasks = CreateQuartlyTasks(year, quarter, outDirectory, taskType);

			   if ( isFirstYear )
			   {
				  quartlyTasks = quartlyTasks.Where ( t => t.Input.Date >= startDate ).ToList ();
			   }

			   foreach ( var task in quartlyTasks )
			   {
				  _allDownloadTasks.Enqueue ( task );
				  _dlStatus.Downloads.Add ( task.Input.Date, TaskStatus.Pending );
				  _dlStatus.Pending++;
			   }
			}

			isFirstYear = false;
		 }
	  }

	  public List<IDownloadTask> CreateQuartlyTasks ( int year, int quarter, string outDirectory, TaskType taskType )
	  {
		 var result = new List<IDownloadTask>();
		 var startMonth = (quarter - 1) * 3 + 1;
		 var endMonth = startMonth + 3;
		 for ( var month = startMonth; month < endMonth; month++ )
		 {
			for ( var day = 1; day <= 31; day++ )
			{
			   var task = CreateTask(year, month, day, quarter, outDirectory, taskType);

			   if ( task != null )
			   {
				  result.Add ( task );
			   }
			}
		 }
		 return result;
	  }

	  IDownloadTask CreateTask ( int year, int month, int day, int quarter, string outDirectory, TaskType taskType )
	  {
		 var url = $"https://www.sec.gov/Archives/edgar/Feed/{year}/QTR{quarter}/{year}{string.Format("{0:00}", month)}{string.Format("{0:00}", day)}.nc.tar.gz";
		 var outputFullPath = Path.Combine(outDirectory, $"{year}_{month}_{day}.nc.tar.gz");
		 DateTime date;

		 try
		 {
			date = new DateTime ( year, month, day );
		 }
		 catch
		 {
			return null;
		 }

		 if ( (taskType == TaskType.Download && File.Exists ( outputFullPath ))
			 || date.DayOfWeek == DayOfWeek.Saturday ||
			  date.DayOfWeek == DayOfWeek.Sunday )
		 {
			return null;
		 }

		 var taskInput = new TaskInput
		 {
			Date = date,
			Url = url,
			OutputPath = outputFullPath
		 };

		 switch ( taskType )
		 {
			case TaskType.Download:
			   return new DownloadTask { Input = taskInput };

			case TaskType.Verify:
			   return new VerifyDownLoadTask { Input = taskInput };

			case TaskType.Create10kSentenments:
			   return new Create10KSentenmentTask { Input = taskInput };

			default:
			   return null;
		 }
	  }

	  void HandleDownloadCompleteEvent ( TaskResult downloadResult )
	  {
		 lock ( _downloadEventLock )
		 {
			_dlResults.Add ( downloadResult );
			_currentThreadCount--;
			UpdateDlStatus ();
		 }

		 TryStartOneTask ();
	  }

	  void UpdateDlStatus ()
	  {
		 if ( _allDownloadTasks.Count <= 0 )
			return;

		 _dlStatus.Successed = _dlResults.Where ( r => r.Status == TaskStatus.Completed ).Count ();
		 _dlStatus.Failed = _dlResults.Where ( r => r.Status == TaskStatus.Failed ).Count ();
		 _dlStatus.Pending = _allDownloadTasks.Count ();

		 _mainForm.UpdateDownLoadStatus ( _dlStatus );
	  }

	  public interface IDownloadTask
	  {
		 void StartTask ();
		 TaskInput Input { get; set; }
	  }


	  public class TaskResult
	  {
		 public TaskStatus Status = TaskStatus.Pending;
	  }

	  public class TaskInput
	  {
		 public DateTime Date;
		 public string OutputPath;
		 public string Url;
		 public string EdgarFile;
	  }
   }
}