﻿namespace EdgarAnalyzer.Tasks
{
	public enum TaskType
	{
		Download,
		Verify,
		Create10kSentenments
	}

	public enum TaskStatus
	{
		Pending,
		Completed,
		Failed
	}
}
