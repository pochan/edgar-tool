﻿using System;
using System.IO;
using static EdgarAnalyzer.TaskProcessor;

namespace EdgarAnalyzer.Tasks
{
	public class VerifyDownLoadTask: DownloadTask
	{
		public override void StartTask()
		{
			Result = new TaskResult();

			try
			{
				var fileInfo = new FileInfo( Input.OutputPath );
				Util.VerifyDownloadedFile( fileInfo );

				Result.Status = TaskStatus.Completed;
			}
			catch ( Exception ex )
			{
				Result.Status = TaskStatus.Failed;
			}

			RaiseTaskCompletedEvent();
		}
	}
}
