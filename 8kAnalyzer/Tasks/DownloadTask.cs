﻿using System;
using System.Net;
using System.Threading;
using static EdgarAnalyzer.TaskProcessor;

namespace EdgarAnalyzer.Tasks
{
	public class DownloadTask: IDownloadTask
	{
		protected static Random Rnd = new Random();
		public TaskInput Input { get; set; }

		protected TaskResult Result;

		public DownloadTask()
		{
			Result = new TaskResult();
		}

		protected void RaiseTaskCompletedEvent()
		{
			TaskCompleted?.Invoke( Result );
		}

		public static event DownloadCompletedEventHandler TaskCompleted;

		public virtual void StartTask()
		{
			// Wait a bit as not to flood the server with requests
			var waitimeMillSec = Rnd.Next( 200, 3000 );
			Thread.Sleep( waitimeMillSec );

			Result = new TaskResult();
			using ( var client = new WebClient() )
			{
				try
				{
					var tmpFile = Input.OutputPath + ".tmp";
					client.DownloadFile( Input.Url, tmpFile );

					if ( Util.MoveFile( tmpFile, Input.OutputPath ) )
						Result.Status = TaskStatus.Completed;
					else
						Result.Status = TaskStatus.Failed;
				}
				catch ( Exception ex )
				{
					Result.Status = TaskStatus.Failed;
				}
			}

			RaiseTaskCompletedEvent();
		}
	}

}
