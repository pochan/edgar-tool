﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdgarAnalyzer.Tasks
{
	public class DownLoadStatus
	{
		public Dictionary<DateTime, TaskStatus> Downloads;

		public int Successed, Failed, Pending;

		public DownLoadStatus()
		{
			Downloads = new Dictionary<DateTime, TaskStatus>();
		}

		public double Progress
		{
			get
			{
				var total = Successed + Failed + Pending;

				return total == 0 ? 0 : ( Successed + Failed ) / (double) total;
			}
		}


		public override string ToString()
		{
			var summary = $"{string.Format( "{0:0.##}", Progress * 100 )}% completed. {Environment.NewLine}";
			summary += $"{Successed} Successfuly downloaded. {Environment.NewLine}";
			summary += $"{Failed} Failed to download. {Environment.NewLine}";
			summary += $"{Pending} Pending. {Environment.NewLine}";
			summary += $"{Successed + Failed + Pending} Total.";
			return summary;
		}
	}


}
