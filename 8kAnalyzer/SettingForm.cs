﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace EdgarAnalyzer
{
	public partial class SettingForm : Form
	{
		public SettingForm()
		{
			InitializeComponent();
			Init();
		}

		void Init()
		{
			numericUpDown1.Value = Properties.Settings.Default.ThreadCount;
			textBox1.Text = Properties.Settings.Default.DefaultEdgarDirectory;
			textBox2.Text = Properties.Settings.Default.RScriptExe;
			textBox3.Text = Properties.Settings.Default.RScriptPath;
			checkBox1.Checked = Properties.Settings.Default.UseRSentenmentAnalyzer;
			defauleColor = textBox2.BackColor;
		}
		Color defauleColor;
		void saveButton_Click( object sender, EventArgs e )
		{
			textBox2.BackColor = defauleColor;
			textBox2.BackColor = defauleColor;
			if ( checkBox1.Checked )
			{
				if ( !File.Exists( textBox2.Text ) )
				{
					MessageBox.Show( "Make sure valid 'RScript.exe' is selected when using R script for sentenment analysis is checked." );
					textBox2.BackColor = Color.Red;
					return;
				}

				if ( !File.Exists( textBox3.Text ) )
				{
					MessageBox.Show( "Make sure a valid R script which performs a sentenment analysis is selected when using R script for sentenment analysis is checked." );
					textBox3.BackColor = Color.Red;
					return;
				}
			}

			Properties.Settings.Default.ThreadCount = (int) numericUpDown1.Value;
			Properties.Settings.Default.DefaultEdgarDirectory = textBox1.Text;
			Properties.Settings.Default.RScriptExe = textBox2.Text;
			Properties.Settings.Default.UseRSentenmentAnalyzer = checkBox1.Checked;
			Properties.Settings.Default.RScriptPath = textBox3.Text;
			Properties.Settings.Default.Save();
			DialogResult = DialogResult.OK;
			Close();
		}

		void cancelButton_Click( object sender, EventArgs e )
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		void button3_Click( object sender, EventArgs e )
		{
			if ( folderBrowserDialog1.ShowDialog() != DialogResult.OK )
			{
				return;
			}
			textBox1.Text = folderBrowserDialog1.SelectedPath;
		}

		private void button4_Click( object sender, EventArgs e )
		{
			if ( openFileDialog1.ShowDialog() != DialogResult.OK )
			{
				return;
			}
			textBox2.Text = openFileDialog1.FileName;
		}

		private void button5_Click( object sender, EventArgs e )
		{
			if ( openFileDialog1.ShowDialog() != DialogResult.OK )
			{
				return;
			}
			textBox3.Text = openFileDialog1.FileName;
		}

		private void checkBox1_CheckedChanged( object sender, EventArgs e )
		{
			if ( checkBox1.Checked && ( string.IsNullOrWhiteSpace( textBox2.Text ) ||
				string.IsNullOrWhiteSpace( textBox3.Text ) ) )
				MessageBox.Show( "Please make sure valid path are set for R script and RScript.exe " );
		}
	}
}
